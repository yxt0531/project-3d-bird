extends Spatial


func _ready():
	$AnimationPlayer.play("RotateCoin")


func _on_Coin_body_entered(body):
	Global.gameplay.spawn_pickup()
	Global.gameplay.add_score()
	self.queue_free()


func _on_Area_OuterRing_body_entered(body):
	Global.gameplay.end_game()
