extends Spatial

var layer1_size
var layer2_size
var layer3_size

export var total_spawn = 6

var current_score
var start_time
var current_time


func _ready():
	Global.gameplay = self
	layer1_size = $SpawnPointsLayer1.get_child_count()
	layer2_size = $SpawnPointsLayer2.get_child_count()
	layer3_size = $SpawnPointsLayer3.get_child_count()
	
	var i = 0
	while i < total_spawn:
		spawn_pickup()
		i += 1
	
	current_score = 0
	start_time = OS.get_unix_time()
	current_time = 0.0
	

func _process(delta):
	current_time = OS.get_unix_time() - start_time
	if Engine.get_time_scale() > 0:
		$CurrentTime.text = "Time: " + str(current_time)
		$CurrentScore.text = "Score: " + str(current_score)
	

func spawn_pickup():
	var pickup_instance = load("res://scenes/systems/pickup.tscn").instance()
	
	randomize()
	pickup_instance.rotate_y(deg2rad(randi() % 179))
	var layer = randi() % 3
	if layer == 0:
		var spawnpoint = $SpawnPointsLayer1.get_child(randi() % layer1_size)
		pickup_instance.translation = spawnpoint.translation + spawnpoint.get_parent().translation
	elif layer == 1:
		var spawnpoint = $SpawnPointsLayer2.get_child(randi() % layer2_size)
		pickup_instance.translation = spawnpoint.translation + spawnpoint.get_parent().translation
	elif layer == 2:
		var spawnpoint = $SpawnPointsLayer3.get_child(randi() % layer3_size)
		pickup_instance.translation = spawnpoint.translation + spawnpoint.get_parent().translation
	else:
		print("Error Spawning Layer")
	
	add_child(pickup_instance)


func end_game():
	Global.save_score(current_score)
	$CurrentScore.visible = false
	$CurrentTime.visible = false
	
	$EndGameScreen.visible = true
	$EndGameScreen/EndScreenLabel.text = "Your Score: " + str(current_score) + "\nHighest Score: " + str(Global.highest_score)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	Engine.time_scale = 0


func add_score():
	current_score += 1
	

func _on_FloorCollisionArea_body_entered(body):
	if body.name == "Player":
		end_game()


func _on_TryAgainButton_pressed():
	Engine.time_scale = 1
	get_tree().reload_current_scene()


func _on_MainMenuButton_pressed():
	Engine.time_scale = 1
	get_tree().change_scene("res://scenes/systems/start_screen.tscn")
