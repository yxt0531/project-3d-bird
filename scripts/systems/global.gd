extends Node

const SAVE_FILE_PATH_GRAPHICS = "user://graphics.setting"
const SAVE_FILE_PATH_SCORE = "user://score"

var gameplay

var save_dict
var save_file

var highest_score


func _ready():
	save_file = File.new()
	
	if not save_file.file_exists(Global.SAVE_FILE_PATH_SCORE):
		save_dict = {
			"highest_score" : 0
		}
		save_file.open(Global.SAVE_FILE_PATH_SCORE, File.WRITE)
		save_file.store_string(to_json(save_dict))
		save_file.close()
		
	save_file.open(Global.SAVE_FILE_PATH_SCORE, File.READ)
	save_dict = parse_json(save_file.get_line())
	
	highest_score = save_dict.get("highest_score")
	
	save_file.close()


func save_score(score):
	if score > highest_score:
		highest_score = score
	
	save_file = File.new()
	
	if save_file.file_exists(Global.SAVE_FILE_PATH_SCORE):
		save_dict = {
			"highest_score" : highest_score
		}
		save_file.open(Global.SAVE_FILE_PATH_SCORE, File.WRITE)
		save_file.store_string(to_json(save_dict))
		save_file.close()

