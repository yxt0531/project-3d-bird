extends Node

var save_dict
var save_file


func _ready():
	save_file = File.new()
	
	if not save_file.file_exists(Global.SAVE_FILE_PATH_GRAPHICS):
		# first time open game, create setting file
		save_dict = {
			# auto resize window to current screen resolution
			"resolution_x" : OS.get_screen_size().x,
			"resolution_y" : OS.get_screen_size().y,
			"vsync" : false,
			"msaa" : Viewport.MSAA_4X
		}
		save_file.open(Global.SAVE_FILE_PATH_GRAPHICS, File.WRITE)
		save_file.store_string(to_json(save_dict))
		save_file.close()
		
	save_file.open(Global.SAVE_FILE_PATH_GRAPHICS, File.READ)
	save_dict = parse_json(save_file.get_line())
	
	OS.set_window_size(Vector2(save_dict.get("resolution_x"), save_dict.get("resolution_y")))
	OS.vsync_enabled = save_dict.get("vsync")
	VisualServer.viewport_set_msaa(get_viewport().get_viewport_rid(), save_dict.get("msaa"))
	
	save_file.close()
	
	center_window()


func resolution_up():
	if not OS.get_borderless_window() and OS.get_window_size().y < OS.get_screen_size().y:
		# if resolution reaches screen resolution, change will be omitted
		if OS.get_window_size().y < 540:
			OS.set_window_size(Vector2(960, 540))
		elif OS.get_window_size().y >= 540 and OS.get_window_size().y < 720:
			OS.set_window_size(Vector2(1280, 720))
		elif OS.get_window_size().y >= 720 and OS.get_window_size().y < 900:
			OS.set_window_size(Vector2(1600, 900))
		elif OS.get_window_size().y >= 900 and OS.get_window_size().y < 1080:
			OS.set_window_size(Vector2(1920, 1080))
		elif OS.get_window_size().y >= 1080 and OS.get_window_size().y < 1440:
			OS.set_window_size(Vector2(2560, 1440))
		elif OS.get_window_size().y >= 1440 and OS.get_window_size().y < 2160:
			OS.set_window_size(Vector2(3840, 2160))
		else:
			OS.set_window_size(Vector2(3840, 2160))
		center_window()


func resolution_down():
	if not OS.get_borderless_window() and OS.get_window_size().y > 540:
		if OS.get_window_size().y < 540:
			OS.set_window_size(Vector2(960, 540))
		elif OS.get_window_size().y > 540 and OS.get_window_size().y <= 720:
			OS.set_window_size(Vector2(960, 540))
		elif OS.get_window_size().y > 720 and OS.get_window_size().y <= 900:
			OS.set_window_size(Vector2(1280, 720))
		elif OS.get_window_size().y > 900 and OS.get_window_size().y <= 1080:
			OS.set_window_size(Vector2(1600, 900))
		elif OS.get_window_size().y > 1080 and OS.get_window_size().y <= 1440:
			OS.set_window_size(Vector2(1920, 1080))
		elif OS.get_window_size().y > 1440 and OS.get_window_size().y <= 2160:
			OS.set_window_size(Vector2(2560, 1440))
		else:
			OS.set_window_size(Vector2(3840, 2160))
		center_window()


func toggle_borderless_fullscreen():
	if not OS.get_borderless_window():
		OS.set_borderless_window(true)
		OS.set_window_size(OS.get_screen_size())
		OS.set_window_position(Vector2(0, 0))
	else:
		OS.set_borderless_window(false)
		center_window()
	
		
func center_window():
	OS.set_window_position((OS.get_screen_size() - OS.get_window_size()) / 2)
	if OS.get_screen_size() < OS.get_window_size():
		OS.set_window_size(OS.get_screen_size())
		OS.set_window_position(Vector2(0, 0))
	
func save():
	save_file = File.new()
	
	if save_file.file_exists(Global.SAVE_FILE_PATH_GRAPHICS):
		save_dict = {
			# auto resize window to current screen resolution
			"resolution_x" : OS.get_window_size().x,
			"resolution_y" : OS.get_window_size().y,
			"vsync" : OS.is_vsync_enabled(),
			"msaa" : Viewport.MSAA_4X
		}
		save_file.open(Global.SAVE_FILE_PATH_GRAPHICS, File.WRITE)
		save_file.store_string(to_json(save_dict))
		save_file.close()
