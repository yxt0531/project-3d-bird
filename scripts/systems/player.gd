extends KinematicBody

const UP = Vector3(0, 1, 0)

const JUMP_SPEED = 5
const GRAVITY = 20
const GRAVITY_CLAMP = 10
const TURN_SPEED_MAX = 100
const TURN_SPEED_ACCEL = 50
const FORWARD_SPEED_MIN = 200
const FORWARD_SPEED_MAX = 1000
const FORWARD_SPEED_ACCEL = 200

const DECEL_MODIFIER = 4

var vec_movement
var rotate_movement
var current_speed


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED) # capture on ready, to be removed
	
	vec_movement = Vector3(0, 0, 0)
	rotate_movement = 0
	current_speed = FORWARD_SPEED_MIN
	
	
func _process(delta):
	if Input.is_action_just_pressed("mouse_release"):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
	if not $PlayerCharacter/AnimationPlayer.is_playing():
		$PlayerCharacter/AnimationPlayer.play("Idle")
	
	
func _physics_process(delta):
	_vertical_movement(delta)
	_rotate_movement(delta)
	_forward_movement(delta)
	
	move_and_slide(vec_movement, UP)
	

func _vertical_movement(delta):
	if is_on_floor():
		vec_movement.y = -GRAVITY_CLAMP
	elif is_on_ceiling():
		vec_movement.y = -GRAVITY_CLAMP
	else:
		vec_movement.y -= GRAVITY * delta
		
	if Input.is_action_just_pressed("pc_jump"):
		vec_movement.y = JUMP_SPEED
		$PlayerCharacter/AnimationPlayer.play("Jump")
		if Engine.get_time_scale() != 0:
			$WingFlap.play()
		
		
func _rotate_movement(delta):
	if Input.is_action_pressed("pc_rotate_right"):
		if rotate_movement > -TURN_SPEED_MAX:
			rotate_movement += -TURN_SPEED_ACCEL * delta
	elif Input.is_action_pressed("pc_rotate_left"):
		if rotate_movement < TURN_SPEED_MAX:
			rotate_movement += TURN_SPEED_ACCEL * delta
	else:
		if rotate_movement > 0:
			rotate_movement -= TURN_SPEED_ACCEL * delta * DECEL_MODIFIER
		elif rotate_movement < 0:
			rotate_movement += TURN_SPEED_ACCEL * delta * DECEL_MODIFIER
		
	rotate_y(deg2rad(rotate_movement * delta))
	

func _forward_movement(delta):
	if Input.is_action_pressed("pc_accel"):
		if current_speed < FORWARD_SPEED_MAX:
			current_speed += FORWARD_SPEED_ACCEL * delta
	elif Input.is_action_pressed("pc_decel"):
		if current_speed > FORWARD_SPEED_MIN:
			current_speed -= FORWARD_SPEED_ACCEL * delta * DECEL_MODIFIER
	
	var local_z = Vector3(0, 0, -current_speed * delta)
	
	# calculate global forward direction
	vec_movement.x = get_transform().basis.xform(local_z).x
	vec_movement.z = get_transform().basis.xform(local_z).z


