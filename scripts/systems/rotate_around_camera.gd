extends Camera

onready var Yaw = get_parent().get_parent()
onready var Pitch = get_parent()

export var fov_zoom = 50
export var fov_default = 70
export var distance = 0

var mouse_sensitivity
var state # if camera is currently zooming, state == 1


func _ready():
	mouse_sensitivity = 0.25
	state = 0
	set_process_input(true)
	set_distance()
	

func _process(delta):
	if state == 0: # zooming back if state == 0
		if fov < fov_default:
			set_fov(fov + delta * 100)


func _look_updown_rotation(rotation = 0):
	var toReturn = Pitch.get_rotation() + Vector3(rotation, 0, 0)
	toReturn.x = clamp(toReturn.x, PI / -2, PI / 2)

	return toReturn


func _look_leftright_rotation(rotation = 0):
	return (Yaw.get_rotation() + Vector3(0, rotation, 0))


func _mouse(event):
	Yaw.set_rotation(_look_leftright_rotation((event.relative.x * mouse_sensitivity) / -200))
	Pitch.set_rotation(_look_updown_rotation((event.relative.y * mouse_sensitivity) / -200))
	
	
func _input(event):
	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		return
	
	elif event is InputEventMouseMotion:
		return _mouse(event)


func set_fov(newFov):
	fov = newFov
	
	
func set_distance():
	if distance < 0:
		self.rotate_y(deg2rad(180))
	self.translation.z = distance
	
	
func zoom():
	state = 1
	if fov > fov_zoom:
		set_fov(fov - get_process_delta_time() * 100)
	
	
func reset(): # external function for reseting camera
	state = 0
