extends Spatial


func _ready():
	$PlayerCharacter/AnimationPlayer.play("Idle")
	$AnimationPlayer.play("Default")
	$VSync.pressed = OS.is_vsync_enabled()


func _on_Button_pressed():
	get_tree().change_scene("res://scenes/systems/help_screen.tscn")


func _on_VSync_toggled(button_pressed):
	if $VSync.pressed:
		OS.vsync_enabled = true
	else:
		OS.vsync_enabled = false
	GraphicsController.save()
